package com.reactlibrary;

import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.Callback;
import java.io.BufferedReader;
import com.facebook.react.bridge.Promise;
import com.facebook.react.common.StandardCharsets;

import android.annotation.TargetApi;
import android.app.Activity;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.os.AsyncTask;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.Socket;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.security.MessageDigest;
import java.text.SimpleDateFormat;

import android.util.Base64;
import java.util.Calendar;
import java.util.Date;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;


import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;


public class RNReactNativePingTcpModule extends ReactContextBaseJavaModule {

  private final ReactApplicationContext reactContext;

    private static SecretKeySpec secretKey;
    private static byte[] key;

  public RNReactNativePingTcpModule(ReactApplicationContext reactContext) {
    super(reactContext);
    this.reactContext = reactContext;
  }


  @Override
  public String getName() {
    return "RNReactNativePingTcp";
  }

    @ReactMethod
    public void  GenerateCryptKey(int timeZone, Callback callback) {
        String cryptKey = "";
        try {
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(new Date());
            calendar.add(Calendar.HOUR_OF_DAY, timeZone);

            for (char c : new SimpleDateFormat("ddMMyyyy").format(calendar.getTime()).toCharArray()) {
                cryptKey += String.valueOf(Integer.parseInt(Character.toString(c)) + Integer.parseInt(Character.toString(c)));
            }
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            byte[] hash = digest.digest(cryptKey.getBytes(StandardCharsets.UTF_8));
            cryptKey = Base64.encodeToString(hash,Base64.NO_WRAP);



        } catch (Exception e) {

        }
        callback.invoke((String) cryptKey);
    }

    @ReactMethod
    public void  GenerateCryptKeyFromString(String timeString, Callback callback) {
        String cryptKey = "";
        try {

            for (char c : timeString.toCharArray()) {
                cryptKey += String.valueOf(Integer.parseInt(Character.toString(c)) + Integer.parseInt(Character.toString(c)));
            }
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            byte[] hash = digest.digest(cryptKey.getBytes(StandardCharsets.UTF_8));
            cryptKey = Base64.encodeToString(hash,Base64.NO_WRAP);



        } catch (Exception e) {

        }
        callback.invoke((String) cryptKey);
    }


    @ReactMethod
    public void genSignKey(int timeZone, int lengthQuery, Callback callback) {


        String SignKey = "";
        try {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.add(Calendar.HOUR_OF_DAY, timeZone);



        String originalSign = "";
        for (char c : new SimpleDateFormat("ddMMyyyy").format(calendar.getTime()).toCharArray()) {
            originalSign += String
                    .valueOf(Integer.parseInt(Character.toString(c)) * lengthQuery);
        }

            System.out.println("originalSign " + originalSign);


        MessageDigest digest = MessageDigest.getInstance("SHA-512");
        byte[] hash = digest.digest(originalSign.getBytes(StandardCharsets.UTF_8));

         SignKey =  Base64.encodeToString(hash, Base64.NO_WRAP); // hash.toString();


            if (!SignKey.equals(Base64.encodeToString(hash, Base64.NO_WRAP))) return;

        } catch (Exception e) {

        }


            callback.invoke ((String) SignKey);


    }

    @ReactMethod
    public void genSignKeyFromString(String timeString, int lengthQuery, Callback callback) {


        String SignKey = "";
        try {


            String originalSign = "";
            for (char c : timeString.toCharArray()) {
                originalSign += String
                        .valueOf(Integer.parseInt(Character.toString(c)) * lengthQuery);
            }

            System.out.println("originalSign " + originalSign);


            MessageDigest digest = MessageDigest.getInstance("SHA-512");
            byte[] hash = digest.digest(originalSign.getBytes(StandardCharsets.UTF_8));

            SignKey =  Base64.encodeToString(hash, Base64.NO_WRAP); // hash.toString();


            if (!SignKey.equals(Base64.encodeToString(hash, Base64.NO_WRAP))) return;

        } catch (Exception e) {

        }


        callback.invoke ((String) SignKey);


    }

    public static void setKey(String myKey)
    {
        MessageDigest sha = null;
        try {
            key = myKey.getBytes("UTF-8");
            sha = MessageDigest.getInstance("SHA-1");
            key = sha.digest(key);
            key = Arrays.copyOf(key, 16);
            secretKey = new SecretKeySpec(key, "AES");
        }
        catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            System.out.println("Error while set key: " + e.toString());
        }
        catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            System.out.println("Error while set key: " + e.toString());
        }
    }

    @ReactMethod
    public void  encrypt(String strToEncrypt, String secret, Callback callback) {
        String encryptText = "";

        try
        {
            setKey(secret);
            Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
            cipher.init(Cipher.ENCRYPT_MODE, secretKey);
            encryptText = Base64.encodeToString(cipher.doFinal(strToEncrypt.getBytes("UTF-8")),Base64.NO_WRAP);
        }
        catch (Exception e)
        {
            System.out.println("[][ex]Error while encrypting: " + e.getMessage());
        }
        callback.invoke((String) encryptText);
    }

    @ReactMethod
    public void  decrypt(String strToDecrypt, String secret, Callback callback) {
        String decryptText = "";
        try
        {
            setKey(secret);
            Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5PADDING");
            cipher.init(Cipher.DECRYPT_MODE, secretKey);
            decryptText = new String(cipher.doFinal(Base64.decode(strToDecrypt,Base64.NO_WRAP)));
        }
        catch (Exception e)
        {
            System.out.println("[][ex]Error while decrypting: " + e.getMessage());
        }

        callback.invoke((String) decryptText);

    }

 @ReactMethod
    public void Connect(final String command, final String Serv, int port,  Callback callback) {


     Socket s = null;
     String resultResponse = null;
     String answer = null;

     if(command == null || command.equals("null")) return;

     try {
         InetAddress serverAddress = InetAddress.getByName(Serv);
         s = new Socket();
         s.connect(new InetSocketAddress(serverAddress, port), 5000);
         s.setSoTimeout(20000);
         s.setSoLinger(true,1000);

         BufferedReader in = null;
         PrintWriter out = null;
         if (s.isConnected()) {
             try {
                 in = new BufferedReader(new InputStreamReader(s.getInputStream()));
                 out = new PrintWriter(new OutputStreamWriter(s.getOutputStream()));
                 out.println(command);
                 out.flush();
                 resultResponse = in.readLine();


             } catch (IOException ioe) {
                 resultResponse = ioe.toString();
             } finally {
                 if (s != null) {
                     try {
                         out.println("{ \"command\":\"terminate\"}");
                         out.flush();

                         out.close();
                         in.close();
                         s.close();
                     } catch (IOException e) {
                         // TODO Auto-generated catch block
                         resultResponse = e.toString();
                     }
                 }
             }
         }else{
             resultResponse = "No connection";
         }
        }
      catch (Exception e)
      {
          resultResponse = e.toString();
      }

     callback.invoke((String) resultResponse);

    }
    
}